/*
 *    Copyright 2015 hongyang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.huawei.mylibrary;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Rect;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Feng Zhaohao
 * Created on 2019/11/10
 */
public class FlowLayout extends ComponentContainer implements Component.EstimateSizeListener, Component.LayoutRefreshedListener {

    private static final String TAG = "FlowLayout";

    private List<Rect> mChildrenPositionList = new ArrayList<>();   // 记录各子 View 的位置
    private int mMaxLines = Integer.MAX_VALUE;      // 最多显示的行数，默认无限制
    private int mVisibleItemCount;       // 可见的 item 数

    public FlowLayout(Context context) {
        super(context);
        this.setEstimateSizeListener(this);
        this.setLayoutRefreshedListener(this);
    }

    public FlowLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        this.setEstimateSizeListener(this);
        this.setLayoutRefreshedListener(this);
    }

    public FlowLayout(Context context, AttrSet attrs,String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setEstimateSizeListener(this);
        this.setLayoutRefreshedListener(this);
    }



    /**
     * 在 wrap_content 情况下，得到布局的测量高度和测量宽度
     * 返回值是一个有两个元素的数组 a，a[0] 代表测量高度，a[1] 代表测量宽度
     */
    private int[] helper(int widthSize) {
        boolean isOneRow = true;    // 是否是单行
        int width = getPaddingLeft();   // 记录当前行已有的宽度
        int height = getPaddingTop();   // 记录当前行已有的高度
        int maxHeight = 0;      // 记录当前行的最大高度
        int currLine = 1;       // 记录当前行数

        for (int i = 0; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            // 获取当前子元素的 margin
            LayoutConfig params = child.getLayoutConfig();
//             mlp;
//            if (params instanceof MarginLayoutParams) {
//                mlp = (MarginLayoutParams) params;
//            } else {
//                mlp = new MarginLayoutParams(params);
//            }
            // 记录子元素所占宽度和高度
            int childWidth =  child.getMarginLeft() + child.getEstimatedWidth() + child.getMarginRight();
            int childHeight =child.getMarginTop() + child.getEstimatedHeight() + child.getMarginBottom();
            maxHeight = Math.max(maxHeight, childHeight);

            // 判断是否要换行
            if (width + childWidth + getPaddingRight() > widthSize) {
                // 加上该行的最大高度
                height += maxHeight;
                // 重置 width 和 maxHeight
                width = getPaddingLeft();
                maxHeight = childHeight;
                isOneRow = false;
                currLine++;
                if (currLine > mMaxLines) {
                    break;
                }
            }
            // 存储该子元素的位置，在 onLayout 时设置
            Rect rect = new Rect(width +child.getMarginLeft(),
                    height + child.getMarginTop(),
                    width + childWidth - child.getMarginRight(),
                    height + childHeight - child.getMarginBottom());
            mChildrenPositionList.add(rect);

            // 加上该子元素的宽度
            width += childWidth;
        }

        int[] res = new int[2];
        res[0] = height + maxHeight + getPaddingBottom();
        res[1] = isOneRow? width + getPaddingRight() : widthSize;

        return res;
    }




    /**
     * 设置 Adapter
     */
    public void setAdapter(Adapter adapter) {
        // 移除之前的视图
        removeAllComponents();
        // 添加 item
        int n = adapter.getItemCount();
        for (int i = 0; i < n; i++) {
            ViewHolder holder = adapter.onCreateViewHolder(this);
            adapter.onBindViewHolder(holder, i);
            Component child = holder.itemView;
            addComponent(child);
        }
    }

    /**
     * 设置最多显示的行数
     */
    public void setMaxLines(int maxLines) {
        mMaxLines = maxLines;
    }

    /**
     * 获取显示的 item 数
     */
    public int getVisibleItemCount() {
        return mVisibleItemCount;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {

            // 清除之前的位置
            mChildrenPositionList.clear();
            // 测量所有子元素（这样 child.getMeasuredXXX 才能获取到值）风险
//            measureChildren(widthMeasureSpec, heightMeasureSpec);

            int widthSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);
            int heightSize =EstimateSpec.getSize(heightMeasureSpec);
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);

            int[] a = helper(widthSize);
            int measuredHeight = 0;
            // EXACTLY 模式：对应指定大小和 match_parent
            if (heightMode == EstimateSpec.PRECISE) {
                measuredHeight = heightSize;
            }
            // AT_MOST 模式，对应 wrap_content
            else if (heightMode == EstimateSpec.NOT_EXCEED) {
                measuredHeight = a[0];
            }
            int measuredWidth = 0;
            if (widthMode == EstimateSpec.PRECISE) {
                measuredWidth = widthSize;
            }
            else if (widthMode == EstimateSpec.NOT_EXCEED) {
                measuredWidth = a[1];
            }

            setEstimatedSize(measuredWidth, measuredHeight);
        return  false;
    }

    @Override
    public void onRefreshed(Component component) {

            // 布置子 View 的位置
            int n = Math.min(getChildCount(), mChildrenPositionList.size());
            for (int i = 0; i < n; i++) {
                Component child = getComponentAt(i);
                Rect rect = mChildrenPositionList.get(i);
                child.setLeft(rect.left);
                child.setRight(rect.right);
                child.setBottom(rect.bottom);
                child.setTop( rect.top);
            }
            mVisibleItemCount = n;

    }

    public abstract static class Adapter<VH extends ViewHolder> {

        public abstract VH onCreateViewHolder(ComponentContainer parent);

        public abstract void onBindViewHolder(VH holder, int position);

        public abstract int getItemCount();

    }

    public abstract static class ViewHolder {
        public final Component itemView;

        public ViewHolder(Component itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.itemView = itemView;
        }
    }
}
