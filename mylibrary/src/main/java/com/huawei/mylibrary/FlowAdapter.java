/*
 *    Copyright 2015 hongyang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.huawei.mylibrary;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.util.List;

public class FlowAdapter extends FlowLayout.Adapter<FlowAdapter.FlowViewHolder> {

    private static final String TAG = "FlowAdapter";

    private Context mContext;
    private List<String> mContentList;

    public FlowAdapter(Context mContext, List<String> mContentList) {
        this.mContext = mContext;
        this.mContentList = mContentList;
    }


    @Override
    public FlowViewHolder onCreateViewHolder(ComponentContainer parent) {
        Component view = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_ability_main,parent,false);
//                LayoutInflater.from(mContext).inflate(R.layout.item_test, parent, false);
        // 给 View 设置 margin
        ComponentContainer.LayoutConfig mlp = new ComponentContainer.LayoutConfig(view.getLayoutConfig());
        int margin = 30;
        mlp.setMargins(margin, margin, margin, margin);
        view.setLayoutConfig(mlp);

        return new FlowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlowViewHolder holder, int position) {
        holder.content.setText(mContentList.get(position));
    }

    @Override
    public int getItemCount() {
        return mContentList.size();
    }

    class FlowViewHolder extends FlowLayout.ViewHolder {
        Text content;

        public FlowViewHolder(Component itemView) {
            super(itemView);
            content = (Text) itemView.findComponentById(ResourceTable.Id_tv_test_content);
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(193,210,240));
            shapeElement.setCornerRadius(25);
//            shapeElement.setShape(ShapeElement.OVAL);
            content.setBackground(shapeElement);
        }
    }
}
