# FlowLayout_ohos

#### 介绍
一种流式布局


#### 项目介绍

- 项目名称：流式布局FlowLayout
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：一种流式布局
- 项目移植状态：核心功能完成
- 调用差异：无
- 项目发起作者：陈丛笑
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 联系方式：isrc_hm@iscas.ac.cn
#### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/151814_3e2a61a6_8539978.png "1111.png")

#### 安装教程

1. 下载library的har包library-debug.har（位于outputs文件夹下）。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，点击“OK”即引用成功。

#### 使用说明

1.  初始化FlowLayout 流式布局，然后初始化list用来呈现内容
```
    private FlowLayout mFlowLayout;
    private List<String> mContentList = new ArrayList<>();
```
2.  给list中添加想要展示的数据
```
     for (int i = 0; i < 4; i++) {
            mContentList.add("java");
            mContentList.add("kotlin");
            mContentList.add("ohos");
            mContentList.add("Deveco-studio");
            mContentList.add("app");
        }
```


3.  把list和FlowLayout使用FlowAdapter联系起来：
```
        mFlowLayout = new FlowLayout(this);
        FlowAdapter adapter = new FlowAdapter(this, mContentList);
        mFlowLayout.setAdapter(adapter);
```
4. 可以设置最多显示的行数：
```
        mFlowLayout.setMaxLines(9);
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 版本和许可信息
FlowLayout_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。