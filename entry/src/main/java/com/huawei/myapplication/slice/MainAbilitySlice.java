/*
 *    Copyright 2015 hongyang
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.huawei.myapplication.slice;


import com.huawei.myapplication.ResourceTable;
import com.huawei.mylibrary.FlowAdapter;
import com.huawei.mylibrary.FlowLayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.LayoutAlignment;

import java.util.ArrayList;
import java.util.List;


public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = "TestActivity";
    private FlowLayout mFlowLayout;
    private List<String> mContentList = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setUIContent(ResourceTable.Layout_ability_main);
        for (int i = 0; i < 4; i++) {
            mContentList.add("java");
            mContentList.add("kotlin");
            mContentList.add("ohos");
            mContentList.add("Deveco-studio");
            mContentList.add("app");
        }
        mFlowLayout = new FlowLayout(this);
        // 设置 Adapter
        FlowAdapter adapter = new FlowAdapter(this, mContentList);
        mFlowLayout.setAdapter(adapter);
        // 设置最多显示的行数
        mFlowLayout.setMaxLines(9);
        // 获取显示的 item 数
//        mFlowLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                Log.d(TAG, "count = " + mFlowLayout.getVisibleItemCount());
//            }
//        });
        super.setUIContent(mFlowLayout);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
